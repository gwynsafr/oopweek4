/*
Ознакомиться с преобразованием типов (type casting, promoting, demoting), типы данных, ofstream, try…catch, throw
1. Напишите программу, которая открывает и выводит на экран содержимое текстового файла. 
При возникновении ошибок, выводите сообщения на экран в соответствии с возникшей ошибкой.
*/

#include <iostream>
#include <regex>
#include <fstream>
#include <string>
#include <vector>

void isfilenamevalid(std::string &file_name) {
    if (file_name.length() == 0) { 
        throw std::invalid_argument("Пустое имя файла");
    }
    std::vector<char> forbidden_symbols = {'<', '>', ':', '"', '/', '\\', '|', '?', '*'};
    for (char forbidden_symbol : forbidden_symbols) {
        if (file_name.find(forbidden_symbol) != std::string::npos) {
            throw std::invalid_argument("В названии использованы запрещённые символы\n");
        }
    }
}

int main() {
    std::string file_name;
    std::string buff;
    std::cout << "Введите название файла с расширением: ";
    std::cin >> file_name;
    isfilenamevalid(file_name);
    try {
        std::ifstream text_file(file_name);
        if (! text_file.good()) {
            throw std::invalid_argument("Не получилось прочитать файл\n");
        }
        text_file >> buff;
        std::cout << buff << std::endl;
        text_file.close();
    }
    catch (std::underflow_error) {
        std::cout << "funny";
    }

    return 0;
}
